#ifndef COMMON_H_INCLUDED
#define COMMON_H_INCLUDED


#include<iostream>
#include <stdio.h>
#include <string.h>
#include<malloc.h>
#include "../ICTCLAS5/API/ICTCLAS50.h"

#include <stdlib.h>
#include <sys/stat.h>            //struct stat,lstat()
#include <sys/types.h>            //opendir()
#include <dirent.h>            //opendir() struct dirent
#include <unistd.h>            //getcwd()
#include<map>
#include<fstream>
#include<sstream>
#include <vector>
#include<algorithm>
#include<numeric>

using namespace std;


#define LENGTH 512


#define ICTCLAS5_PATH "/home/fuxiang/bwf/ICTCLAS5/API"
#define ROOT_PATH "/home/fuxiang/bwf/"
#define ROOT_RESULT_PATH "/home/fuxiang/bwf/output/"
#define INDEX_IN_ALL_WEB "/home/fuxiang/bwf/output/index.txt"
#define DIRPATH_ALL_WEB "/home/fuxiang/bwf/data/test2/"
#define ID_KEY_PATH "/home/fuxiang/bwf/data/id_key"
#define MAX_WORD_NUM 200000
#define SUM 0


#endif // COMMON_H_INCLUDED
