#include "function.h"

void split(const string& src, const string& separator, vector<string>& dest)
{
    string str = src;
    string substring;
    string::size_type start = 0, index;

    do
    {
        index = str.find_first_of(separator,start);
        if (index != string::npos)
        {
            substring = str.substr(start,index-start);
            dest.push_back(substring);
            start = str.find_first_not_of(separator,index);
            if (start == string::npos) return;
        }
    }
    while(index != string::npos);

    //the last token
    substring = str.substr(start);
    dest.push_back(substring);
}

int isDir(char *name)
{
    struct stat buff;

    if(lstat(name,&buff)<0)
        return 0;        //if not exist name ,ignore.
    //if is directory return 1,else return 0
    return S_ISDIR(buff.st_mode);
}

bool isbig(map<string,float>::iterator it1,map<string,float>::iterator it2)
{
    return (it1)->second  - (it2)->second > 0.0000001;
}
