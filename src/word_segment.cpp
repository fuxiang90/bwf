#include "word_segment.h"


/*
直接传入一个分词目录 就可以递归的进行分词
*/
void search_file(char *path)
{
    DIR * directory;
    struct dirent * dir_entry;
    char buffer[LENGTH];
    if((directory=opendir(path))==NULL)
    {
        fprintf(stderr,"%s",path);
        perror(" ");
        return ;
    }
    while((dir_entry=readdir(directory)))
    {
        if(!strcmp(dir_entry->d_name,".")||!strcmp(dir_entry->d_name,".."))
        {
            //do nothing
        }

        else
        {

            if((strcmp(path,"/"))==0)
                sprintf(buffer,"%s%s",path,dir_entry->d_name);

            else
                sprintf(buffer,"%s/%s",path,dir_entry->d_name);

            if(isDir(buffer))
                search_file(buffer);
            else
            {
                if(!strstr(buffer,"segment"))
                {
                    string temp_buffer=buffer;
                    string store_path=temp_buffer+".segment";
                    ICTCLAS_FileProcess(buffer,store_path.c_str(), CODE_TYPE_UTF8,0);
                    remove(buffer);
                }
            }
        }
    }
    closedir(directory);
}

int word_segment_main(char *path)
{
    if(!ICTCLAS_Init(ICTCLAS5_PATH))
    {
        printf("Init fails\n");
        return -1;
    }
    ICTCLAS_SetPOSmap(PKU_POS_MAP_FIRST);
    string tempdir(path);
    tempdir = DIRPATH_ALL_WEB + tempdir;

    search_file(tempdir.c_str());
    ICTCLAS_Exit();
    return 0;
}

//对全网的进行分词
int word_segment_entire()
{
    if(!ICTCLAS_Init(ICTCLAS5_PATH))
    {
        printf("Init fails\n");
        return -1;
    }
    ICTCLAS_SetPOSmap(PKU_POS_MAP_FIRST);

    search_file(DIRPATH_ALL_WEB);
    ICTCLAS_Exit();
    return 0;
}
