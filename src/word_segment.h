#ifndef WORD_SEGMENT_H_INCLUDED
#define WORD_SEGMENT_H_INCLUDED

#include "common.h"
#include "function.h"

void search_file(char *path);

int word_segment_main(char *path);

int word_segment_entire();
#endif // WORD_SEGMENT_H_INCLUDED
