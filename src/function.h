#ifndef FUNCTION_H_INCLUDED
#define FUNCTION_H_INCLUDED

#include "common.h"
/*
分割字符串
*/
void split(const string& src, const string& separator, vector<string>& dest);

/*
检测一个文件名是否目录
*/
int isDir(char *name);

bool isbig(map<string,float>::iterator it1,map<string,float>::iterator it2);
#endif // FUNCTION_H_INCLUDED
