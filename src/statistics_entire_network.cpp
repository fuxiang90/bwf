
#include "statistics_entire_network.h"
#define LENGTH 512

static unsigned long  static_sum ;

int st_entire_network(char *path  , string index_path)
{
    map<string,int> text_map;
    DIR * directory;
    struct dirent * dir_entry;
    string index_path1= index_path;
    ofstream fout (index_path.c_str());
    char buffer[LENGTH];
    if((directory=opendir(path))==NULL)
    {
        fprintf(stderr,"%s",path);
        perror(" ");
        return 0;
    }
    while((dir_entry=readdir(directory)))
    {
        if(!strcmp(dir_entry->d_name,".")||!strcmp(dir_entry->d_name,".."))
        {
            //do nothing
        }
        else
        {

            if((strcmp(path,"/"))==0)
                sprintf(buffer,"%s%s",path,dir_entry->d_name);

            else
                sprintf(buffer,"%s/%s",path,dir_entry->d_name);

            if(isDir(buffer))
            {
                struct dirent * dir_entry_temp;
                DIR * directory_temp;
                if((directory_temp=opendir(buffer))==NULL)
                {
                    fprintf(stderr,"%s",buffer);
                    perror(" ");
                    return 0;
                }
                while((dir_entry_temp=readdir(directory_temp)))
                {
                    if(!strcmp(dir_entry_temp->d_name,".")||!strcmp(dir_entry_temp->d_name,".."))
                    {
                        //do nothing
                    }
                    else
                    {

                        FILE *fp;
                        char file_path[LENGTH];
                        strcpy(file_path,buffer);
                        strcat(file_path,"/");
                        strcat(file_path,dir_entry_temp->d_name);
                        fp = fopen(file_path, "r");
                        fseek( fp , 0 , SEEK_END );
                        int file_size;
                        file_size = ftell( fp );
                        char *tmp;
                        fseek( fp , 0 , SEEK_SET);
                        tmp =  (char *)malloc( file_size * sizeof( char ) );
                        fread( tmp , file_size , sizeof(char) , fp);
                        string str=tmp;
                        string str1=" ";
                        vector<string> vc;
                        split(str,str1,vc);
                        vector<string>::iterator it;
                        for(it=vc.begin(); it!=vc.end(); it++)
                        {
                            //if((*it).length()>5 &&((*it).find("/n")<100 || (*it).find("/v")<100))
                            if((*it).length()>5 )
                            {
                                if(text_map.find(*it) == text_map.end())
                                {
                                    text_map.insert(map<string,int>::value_type(*it,0) );
                                }
                                text_map[*it]++;

                            }
                        }
                    }
                }
            }
        }
    }
    static_sum = 0;
    typedef struct st
    {
        string s;
        int ct;
    } st;
    st sort_map[MAX_WORD_NUM];
    int i=0,j,k;
    for(map<string,int>::iterator it=text_map.begin(); it!=text_map.end(); it++)
    {
        //fout<<(it->first)<<" "<<it->second<<endl;
        sort_map[i].s=it->first;
        sort_map[i].ct=it->second;
        static_sum+=it->second;//bug
        i++;
//        if(i>Max_Words_Num-1)
//        {
//            st * p=sort_map;
//            int num=(int)Max_Words_Num;
//            p=(st *)realloc(p,Max_Words_Num*2);
//           // Max_Words_Num=num;
//        }
    }
    for(j=0; j<i; j++)
    {
        for(k=j+1; k<i; k++)
        {
            if(sort_map[j].ct<sort_map[k].ct)
            {
                st q=sort_map[j];
                sort_map[j]=sort_map[k];
                sort_map[k]=q;
            }
        }
    }
    for(j=0; j<i; j++)
    {
        fout<<sort_map[j].s<<' '<<sort_map[j].ct<<endl;
    }
    return 0;
}
void st_entire_network_main()
{
    word_segment_entire();
    st_entire_network(DIRPATH_ALL_WEB ,INDEX_IN_ALL_WEB);
}
