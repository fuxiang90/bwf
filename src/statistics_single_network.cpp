
#include "common.h"
#include "function.h"

#include "statistics_single_network.h"
#include "word_segment.h"

#define LENGTH 512

int st_single_network(char *url  ,  vector<string> &core_vc )
{

    map<string,int> text_map;
    DIR * directory;
    char path[LENGTH] = {NULL};
//    strcpy(path ,"\0");
    strcat(path,DIRPATH_ALL_WEB);
    strcat(path,url);
    struct dirent * dir_entry;
    char buffer[LENGTH];
    if((directory=opendir(path))==NULL)
    {
        fprintf(stderr,"%s",path);
        perror(" ");
        return 0;
    }
    char dest_file[LENGTH];
    sprintf(dest_file ,"%s%s.txt",ROOT_RESULT_PATH,url);

    string dest(dest_file);
    ofstream final_fout(dest.c_str());
    while((dir_entry=readdir(directory)))
    {
        if(!strcmp(dir_entry->d_name,".")||!strcmp(dir_entry->d_name,".."))
        {
            //do nothing
        }
        else
        {
            if((strcmp(path,"/"))==0)
                sprintf(buffer,"%s%s",path,dir_entry->d_name);

            else
                sprintf(buffer,"%s/%s",path,dir_entry->d_name);

            if(isDir(buffer))
            {
                //do nothing
            }
            else
            {
                FILE *fp;
                fp = fopen( buffer, "r");
                fseek( fp , 0 , SEEK_END );
                int file_size;
                file_size = ftell( fp );
                char *tmp;
                fseek( fp , 0 , SEEK_SET);
                tmp =  (char *)malloc( file_size * sizeof( char ) );
                fread( tmp , file_size , sizeof(char) , fp);
                string str=tmp;
                string str1=" ";
                vector<string> vc;
                split(str,str1,vc);
                vector<string>::iterator it;
                for(it=vc.begin(); it!=vc.end(); it++)
                {
                    string tem_str=*it;
                    if(tem_str.length()>3 )
                    {
                        if(text_map.find(*it) == text_map.end())
                        {
                            text_map.insert(map<string,int>::value_type(*it,0) );
                        }
                        text_map[*it]++;
                    }
                }
            }
        }
    }
    float sum=0;
    for(map<string,int>::iterator it=text_map.begin(); it!=text_map.end(); it++)
    {
        //final_fout<<it->first<<' '<<it->second<<endl;
        sum+=it->second;
    }
    if(!ICTCLAS_Init(ICTCLAS5_PATH))
    {
        printf("Init fails\n");
        return -1;
    }
    ICTCLAS_SetPOSmap(PKU_POS_MAP_FIRST);

//    string dest=ROOT_RESULT_PATH+url+".txt";

//    string dest = url;
//    dest = ROOT_RESULT_PATH + dest;
//    dest = dest + ".txt";


    string x="";
    vector<string> temp_vc;
    map<string,float> temp_core_key_map;
    vector<string>::iterator core_it;
    for(core_it=core_vc.begin(); core_it!=core_vc.end(); core_it++)
    {
        string x=*core_it;
        temp_vc.push_back(x);
        const char* sSentence=x.c_str();
        int nPaLen=strlen(sSentence);
        char* sRst=0;//用户自行分配空间，用于保存结果；
        sRst=(char *)malloc(nPaLen*6);//建议长度为字符串长度的6倍。
        ICTCLAS_ParagraphProcess(sSentence,nPaLen,sRst,CODE_TYPE_UTF8,0);

        string tmp=sRst;
        string temp_str1=" ";
        vector<string> temp_vd;
        split(tmp,temp_str1,temp_vd);
        vector<string>::iterator temp_it;
        int core_key_sum=1;
        int word_sum=0;
        int add_weight_num=1;
        for(temp_it=temp_vd.begin(); temp_it!=temp_vd.end(); temp_it++)
        {
            //if((*temp_it).length()>5  &&((*temp_it).find("/n")<100 || (*temp_it).find("/v")<100))
            if((*temp_it).length()>5 )
            {
                word_sum++;
                string str=*temp_it;
                if(text_map.count(str))
                {
                    add_weight_num++;
                    int num=text_map[str];
                    if(num>3)
                    {
                        num*=2;
                    }
                    core_key_sum+=num;
                }
            }
        }
        float result = 0;
        if(sum && word_sum){
            result=add_weight_num*core_key_sum/(sum*word_sum);

        }

        temp_core_key_map[x]=result;
    }
    map<string,float>::iterator final_it;
    for(final_it=temp_core_key_map.begin(); final_it!=temp_core_key_map.end(); final_it++)
    {
        final_fout<<final_it->first<<" "<<final_it->second<<endl;
        //cout<<final_it->first<<" "<<final_it->second<<endl;
    }
    ICTCLAS_Exit();
    return 0;
}
int st_single_network_main(int url_id)
{

    // get info

    string url_str = "www.010beiqinglv.com" ;
    url_id = 1234;
    word_segment_main(url_str.c_str());

    //st_single_network("../save_new/zjhengguang" ,"./index.txt" ,"./core/zjhengguang.txt");
    vector<string> vc;
    char buf[LENGTH];

    FILE *fin = fopen(ID_KEY_PATH ,"r");
    int id ;
    while(fscanf(fin,"%d%s",&id,buf)!=EOF){

        if(id == url_id){
            vc.push_back(buf);
        }
    }
//    vc.push_back("北京旅游");
//    vc.push_back("北京两日游");
//    vc.push_back("北京户外");
//    vc.push_back("长城");
//    vc.push_back("故宫");

    st_single_network(url_str.c_str() ,vc);
    return 1;


}

