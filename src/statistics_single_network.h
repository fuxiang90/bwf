#ifndef ST_SINGLE_NETWORK_H_INCLUDED
#define ST_SINGLE_NETWORK_H_INCLUDED



int st_single_network_main(int url_id);
/*
url: each web's url
vc:each web's core key
result_path: strore result
*/
int st_single_network(char *url  , vector<string> &core_vc );

int st_single_get_info(int url_id ,char * id_url_file ,char * id_key_file ,string & url_str ,vector<string> & key_vec);
#endif // ST_SINGLE_NETWORK_H_INCLUDED
