# -*- coding: utf-8-*-
import re
import os
import argparse
##过滤HTML中的标签
#将HTML中标签等信息去掉
#@param htmlstr HTML字符串.
def filter_tags(htmlstr):
    #先过滤CDATA
    re_cdata=re.compile('//<!\[CDATA\[[^>]*//\]\]>',re.I) #匹配CDATA
    re_script=re.compile('<\s*script[^>]*>[^<]*<\s*/\s*script\s*>',re.I)#Script
    re_style=re.compile('<\s*style[^>]*>[^<]*<\s*/\s*style\s*>',re.I)#style
    re_br=re.compile('<br\s*?/?>')#处理换行
    re_h=re.compile('</?\w+[^>]*>')#HTML标签
    re_comment=re.compile('<!--[^>]*-->')#HTML注释
    s=re_cdata.sub('',htmlstr)#去掉CDATA
    s=re_script.sub('',s) #去掉SCRIPT
    s=re_style.sub('',s)#去掉style
    s=re_br.sub('\n',s)#将br转换为换行
    s=re_h.sub('',s) #去掉HTML 标签
    s=re_comment.sub('',s)#去掉HTML注释
    #去掉多余的空行
    blank_line=re.compile('\n+')
    s=blank_line.sub('\n',s)
    s=replaceCharEntity(s)#替换实体
    return s

##替换常用HTML字符实体.
#使用正常的字符替换HTML中特殊的字符实体.
#你可以添加新的实体字符到CHAR_ENTITIES中,处理更多HTML字符实体.
#@param htmlstr HTML字符串.
def replaceCharEntity(htmlstr):
    CHAR_ENTITIES={'nbsp':' ','160':' ',
                'lt':'<','60':'<',
                'gt':'>','62':'>',
                'amp':'&','38':'&',
                'quot':'"','34':'"',}
   
    re_charEntity=re.compile(r'&#?(?P<name>\w+);')
    sz=re_charEntity.search(htmlstr)
    while sz:
        entity=sz.group()#entity全称，如&gt;
        key=sz.group('name')#去除&;后entity,如&gt;为gt
        try:
            htmlstr=re_charEntity.sub(CHAR_ENTITIES[key],htmlstr,1)
            sz=re_charEntity.search(htmlstr)
        except KeyError:
            #以空串代替
            htmlstr=re_charEntity.sub('',htmlstr,1)
            sz=re_charEntity.search(htmlstr)
    return htmlstr

def repalce(s,re_exp,repl_string):
    return re_exp.sub(repl_string,s)

#传入的必须是带path的file 或者path 目录
def distinguish_file(user_path):

    if os.path.isfile(user_path):
        #s=file(user_path).read()
        #如果是做完分词的就不进行去tab 操作了
        if user_path.endswith('segment') == True : 
            return 
        s=open(user_path).read()
        news=filter_tags(s)
        # str1='./save_remove_html/'
        # str2=str1+user_path
        dir,f=os.path.split(user_path)
        if not os.path.exists(dir):
            #os.makedirs(dir,0777)
            os.makedirs(dir)
        f=open(user_path,'w')
        f.write(news)
        f.close()

        cmd = 'tr -d "[a-z][A-Z][0-9](){}_.=$+-<>" < %s | tr -d "\n"|tr -d "\t" |tr -d " "| tr -d "[\r]" ' %user_path
        r = os.popen(cmd).read()


        f=open(user_path,'w')
        f.write(r)
        f.close()

    else:
        #os.chdir(user_path) 
        # print('%s is a dir'%user_path)
        search_file = os.listdir(user_path)
        for index in search_file:
            distinguish_file(user_path+"/"+index)
        # if os.path.isdir(user_path+"/"+index):
        	#print "%s is dir" % (user_path+"/"+index),os.getcwd()


if __name__=='__main__':
   ##s=file('./save/f00002').read()
   ##news=filter_tags(s)
   # news=news.split('\n')
   # news=''.join(news)
   # news=news.split('\t')
   # news=''.join(news)
   # news=news.strip()
   # news=news.lstrip()
   # news=news.rstrip()
#    news.replace('\n','').replace('\t','')
#    news=news.split(' ')
#    news=''.join(news)
#    news=news.strip()
#    news=news.lstrip()
#    news=news.rstrip()

#    print news
    ##f=open('./save_remove_html/f00002','w')
    ##f.write(news)
    
    
    parser = argparse.ArgumentParser(description='handle',formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-p','--path_name',help='论坛板块的名字')
    args = parser.parse_args()
    path_name = args.path_name
    distinguish_file(path_name)


    print "done it"
