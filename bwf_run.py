#! coding=utf-8
#! /bin/python

"""
需要 配置larbin 下载下来的web 目录

入口函数  
1 传入 网页url  网页id   然后根据 url 就可以找到 对应的目录 进行转码 切分


"""
from tool.gbtutf import path,gbk_utf_filename
from tool.remove_html  import distinguish_file
import os
import argparse


class bwf_run(object):


    def __init__(self,url,url_id,mode = 'full',conf='bwf_run.conf'):
        self._url = url
        self._url_id = url_id 
        self._mode = mode
        self._conf = conf
        self._path = "./data/test2/" + url;
        self._data_path = './data/test2/'

    
    def _process_path(self,path):
        ''' 把某个path 下面的文件 转码 去 html tab'''
        
        file_names = os.listdir(path)
        for file_name in file_names:
            new_file_name = gbk_utf_filename(os.path.join(path ,file_name) )
            distinguish_file(new_file_name)
        
    def run(self):
        
        # full 模式还尚待测试

        if self._mode == 'full':
            pathnames = os.listdir(self._data_path)
            for pathname in pathnames:
                #print pathname
                path = os.path.join(self._data_path,pathname)
                self._process_path(path)
            
            cmd = './src/bin/Debug/main_project -m full' 
            r = os.popen(cmd).read() 
            print r 
        if self._mode == 'single': 
            self._process_path(self._path)
            
            #cmd = './src/bin/Debug/main_project -i %s' %(self._url_id)
            #r = os.popen(cmd).read() ;print r






if __name__ =='__main__':
    

    parser = argparse.ArgumentParser(description='handle',
                                                            formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-m','--mode',help='代码运行的模式')

    parser.add_argument('-i' ,'--url_id',help='提供url对应的id' )

    parser.add_argument('-c' ,'--conf' ,help='conf文件的地址')
    args = parser.parse_args() 
    # config 
    id = 0
    conf = ""
    mode = "full" #'single' 'full' 'file'
    if args.url_id :
        id = args.url_id
    if args.conf:
        conf = args.conf

    if args.mode :
        mode = args.mode


    id = 12

    test = bwf_run('www.010beiqinglv.com',12,mode)
    test.run()
    print "done it"




